package com.example.planetest.db;


import java.util.ArrayList;

public class Sql {
    public static String createDb(String name) {
        return "CREATE DATABASE IF NOT EXISTS " + name + ";";
    }

    public static String createTable(String tableName, ArrayList<Table> tableConfig) {
        StringBuilder query = new StringBuilder("CREATE TABLE IF NOT EXISTS " + tableName + " (");

        for(Table item: tableConfig) {
            query.append(item.field).append(" ").append(item.type);

            if(item.isNotNull) {
                query.append(" " + "NOT NULL");
            }

            if(item.primaryKey != null) {
                query.append(" " + "PRIMARY KEY (").append(item.primaryKey).append(")");
            }

            query.append(",");
        }

        query.deleteCharAt(query.length() -1);
        query.append(");");
        return query.toString();
    }
}
