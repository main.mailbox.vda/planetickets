package com.example.planetest.db;

public class Table {
    /**
     * Field name
     */
    String field = "";

    /**
     * CHAR(n), VARCHAR(n), BOOL, INTEGER, DECIMAL
     */
    String type = "";

    /**
     * if > - 1 to set PRIMARY KEY (field)
     */
    String primaryKey = null;

    /**
     * if true to set NOT NULL
     */
    boolean isNotNull = false;

    Table(String field, String type, String primaryKey, boolean isNotNull){
        this.field = field;
        this.type = type;
        this.primaryKey = primaryKey;
        this.isNotNull = isNotNull;
    }
}
