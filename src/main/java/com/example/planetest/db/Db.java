package com.example.planetest.db;

import java.sql.*;
import java.util.ArrayList;

public class Db {
    private static Connection connection = null;

    public Db() {
        if (Db.connection == null) {
            try{
                String url = "jdbc:mysql://localhost:3306/PlaneTickets";
                String username = "root";
                String password = "1234";
                Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
                try (Connection conn = DriverManager.getConnection(url, username, password)){
                    this.connection = conn;
                    createDb();
                    System.out.println("Connection to Store DB successful!");
                }
            }
            catch(Exception ex){
                System.out.println("Connection failed...");
                System.out.println(ex);
            }
        } else {
            System.out.println("Connection already exist");
        }
    }

    public void createDb() throws SQLException {
        Statement statement = Db.connection.createStatement();
        statement.executeUpdate(Sql.createDb("PlaneTickets"));
        statement.execute("use PlaneTickets");
        statement.close();
    }
}
