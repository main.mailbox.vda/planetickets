package com.example.planetest;

import com.example.planetest.db.Db;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;

public class PlaneTicketsApplication extends Application {
    private Stage window;

    public static void main(String[] args) {
        launch();
    }

    @Override
    public void start(Stage stage) throws IOException {
        window = stage;
        initialData();
        initialScene();
    }

    private void initialScene() throws IOException {
        window.setTitle("Plane tickets");
        FXMLLoader welcomeFxmlLoader = new FXMLLoader(PlaneTicketsApplication.class.getResource("welcome.fxml"));
        window.setScene(new Scene(welcomeFxmlLoader.load()));
        window.show();
    }

    private void initialData() {
        new Db();
    }
}