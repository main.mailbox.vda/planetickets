package com.example.planetest.welcome;

import com.example.planetest.PlaneTicketsApplication;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class WelcomeController  {
    @FXML
    Button btnSignIn, btnFind;

    @FXML
    protected void onSignInClick() throws IOException {
        Stage window = (Stage) btnSignIn.getScene().getWindow();
        FXMLLoader authFxmlLoader = new FXMLLoader(PlaneTicketsApplication.class.getResource("auth.fxml"));
        window.setScene(new Scene(authFxmlLoader.load()));
    }

    @FXML
    protected void onFindTicketClick() {
        System.out.printf("onFindTicketClick");
    }
}