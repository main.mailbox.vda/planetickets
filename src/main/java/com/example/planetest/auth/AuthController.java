package com.example.planetest.auth;

import com.example.planetest.PlaneTicketsApplication;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class AuthController {
    @FXML
    Button btnBack;

    @FXML
    protected void onContinueClick() {
        System.out.printf("onSignInClick");
    }

    @FXML
    protected void onBackClick() throws IOException {
        Stage window = (Stage) btnBack.getScene().getWindow();
        FXMLLoader authFxmlLoader = new FXMLLoader(PlaneTicketsApplication.class.getResource("welcome.fxml"));
        window.setScene(new Scene(authFxmlLoader.load()));
    }
}
