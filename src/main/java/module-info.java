module com.example.planetest {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires validatorfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires eu.hansolo.tilesfx;
    requires java.sql;
    requires mysql.connector.java;

    opens com.example.planetest to javafx.fxml;
    exports com.example.planetest;
    exports com.example.planetest.auth;
    opens com.example.planetest.auth to javafx.fxml;
    exports com.example.planetest.welcome;
    opens com.example.planetest.welcome to javafx.fxml;
}